/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <iostream>
#include <stdio.h>
#include <immintrin.h> // Required to use intrinsic functions
#include <math.h>
#include <CImg.h>
using namespace std;
using namespace cimg_library;

// TODO: Example of use of intrinsic functions
// This example doesn't include any code about image processing


#define ITEMS_PER_PACKET (sizeof(__m128d)/sizeof(double))

typedef double data_t;
const char* SOURCE_IMG      = "bailarina.bmp";
const char* SOURCE_IMG_NEGRO= "background_V.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

int main() {
	
	// Data arrays to sum. May be or not memory aligned to __m256 size (32 bytes)
    //double src[VECTOR_SIZE];

    CImg<data_t> srcImage2(SOURCE_IMG);
    CImg<data_t> srcImage(SOURCE_IMG_NEGRO);

    data_t *pRsrcnegro, *pGsrcnegro, *pBsrcnegro; // Pointers to the R, G and B components
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image
    uint widthnegro, heightnegro; // Width and height of the image
	uint nComp; // Number of image components
    
    srcImage.display(); // Displays the source image
	widthnegro  = srcImage.width(); // Getting information from the source image
	heightnegro = srcImage.height();

    struct timespec tStart, tEnd;
	double dElapsedTimeS;

	srcImage2.display(); // Displays the source image
	width  = srcImage2.width(); // Getting information from the source image
	height = srcImage2.height();
	nComp  = srcImage2.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)
	const uint VECTOR_SIZE=height*width;
	int nPackets=(VECTOR_SIZE * sizeof(data_t)/sizeof(__m128d));
	if(((VECTOR_SIZE*sizeof(data_t))%sizeof(__m128d))!=0)
		nPackets++;
	// Allocate memory space for destination image components
	pDstImage = (data_t *)_mm_malloc(sizeof(__m128d)*(nComp*nPackets), sizeof(__m128d));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
 	pRsrcnegro = srcImage.data(); // pRcomp points to the R component array
	pGsrcnegro = pRsrcnegro + heightnegro * widthnegro; // pGcomp points to the G component array
	pBsrcnegro = pGsrcnegro + heightnegro * widthnegro; // pBcomp points to B component array

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage2.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

    if(clock_gettime(CLOCK_REALTIME, &tStart) == -1){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	// data_t vector255[ITEMS_PER_PACKET];
	// data_t vector256[ITEMS_PER_PACKET];
	// data_t vector1[ITEMS_PER_PACKET];
    __m128d va, vb, vc ,vresult;
	va = _mm_set1_pd(255.0); 
	vb = _mm_set1_pd(256.0);
	vc = _mm_set1_pd(1.0);
	uint repeticiones = 15;

	// for(int i=0;i<ITEMS_PER_PACKET;i++){
	// 	*(vector255+i)=255.0f;
	// 	*(vector256+i)=256.0f;
	// 	*(vector1+i)=1.0f;
	// }
	// va=_mm_load_pd(vector255);
	// vb=_mm_load_pd(vector256);
	// vc=_mm_load_pd(vector1);
    for (uint k = 0; k < repeticiones; k++){
        for (uint i = 0; i < width*height; i+=ITEMS_PER_PACKET){    
			vresult=_mm_sub_pd(va,_mm_div_pd(_mm_mul_pd(vb,(_mm_sub_pd(va,(_mm_load_pd(pRsrcnegro+i))))),_mm_add_pd(_mm_load_pd(pRsrc+i),vc)));
            *(__m128d *)(pRdest+i)=vresult;
			vresult=_mm_sub_pd(va,_mm_div_pd(_mm_mul_pd(vb,(_mm_sub_pd(va,(_mm_load_pd(pGsrcnegro+i))))),_mm_add_pd(_mm_load_pd(pGsrc+i),vc)));
            *(__m128d *)(pGdest+i)=vresult;
			vresult=_mm_sub_pd(va,_mm_div_pd(_mm_mul_pd(vb,(_mm_sub_pd(va,(_mm_load_pd(pBsrcnegro+i))))),_mm_add_pd(_mm_load_pd(pBsrc+i),vc)));
            *(__m128d *)(pBdest+i)=vresult;	
        }
    }
	for(int i=0;i<height*width;i++){
		if(*(pRdest + i) < 0){
			*(pRdest + i) = 0;
		}
		//*(pGdest + i) = 255.0-(((255.0-(*(pGsrcnegro+i)))*256.0)/(*(pGsrc+i)+1.0));
		if(*(pGdest + i) < 0){
			*(pGdest + i) = 0;
		}
		//*(pBdest + i) = 255.0-(((255.0-(*(pBsrcnegro+i)))*256.0)/(*(pBsrc+i)+1.0));
		if(*(pBdest + i) < 0){
			*(pBdest + i) = 0;
		}
	}

    if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

		
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
    _mm_free(pDstImage);
    cout << "Tiempo final: " << dElapsedTimeS ;
	return 0;
}